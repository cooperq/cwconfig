import re

def filter(raw, canary):
    clean = re.sub(re.compile('As of.+$', re.DOTALL), '', raw)
    return clean
