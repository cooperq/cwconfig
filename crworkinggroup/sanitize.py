from pyquery import PyQuery as pq

def filter(raw_text, canary):
    d = pq(raw_text) # d is similar to $ in jQuery
    content = d('#content').text()
    # Strip out non latin-1 characters since the page *says* it's latin-1 encoded.
    return ''.join(i for i in content if ord(i)<256) 

