import re
from pyquery import PyQuery as pq

def filter(raw_text, canary):
    d = pq(raw_text) #d is like $ in jquery
    content = d('#blog-post').text()
    # Strip out non ascii characters since the page *says* it's ascii encoded.
    return ''.join(i for i in content if ord(i)<128)

