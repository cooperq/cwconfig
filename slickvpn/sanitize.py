import re
from pyquery import PyQuery as pq

def filter(raw_text, canary):
    d = pq(raw_text) #d is like $ in jquery
    content = d('.std').text()
    content = re.search('As of.+performed on SlickVPN assets.', content, re.DOTALL).group(0)
    return content

