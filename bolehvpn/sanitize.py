import re
from pyquery import PyQuery as pq

def filter(raw, canary):
    d = pq(raw) #d is like $ in jquery
    content = d('body').text()
    content = re.search('.....BEGIN PGP SIGNED MESSAGE.....(.*).....BEGIN PGP SIGNATURE.....', content, re.DOTALL).group(1)
    return content
