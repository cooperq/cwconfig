import re

def filter(raw, canary):
    clean = re.sub(re.compile('The following news headlines.+', re.DOTALL), '', raw)
    return clean
