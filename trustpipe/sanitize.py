from pyquery import PyQuery as pq

def filter(raw_text, canary):
    d = pq(raw_text) #d is like $ in jquery
    content = d('.neg-dec').text()
    return ''.join(i for i in content if ord(i)<128)

