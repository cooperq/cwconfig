import re

def filter(raw, canary):
    clean = re.sub(re.compile('You are viewing this page on.+$', re.DOTALL), '', raw)
    return clean
