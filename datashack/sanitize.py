import re

def filter(raw_text, canary):
    return  re.search('We do not participate in any government mass collection of data.  If this provision disappears you will know we have been involuntarily compelled to take part in such a program.', raw_text, re.DOTALL).group(0)

