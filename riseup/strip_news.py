import re

def filter(raw, canary):
    clean = re.sub(re.compile('(once per quarter\.\s*)(.+)$', re.DOTALL), r'\1', raw)
    return clean
