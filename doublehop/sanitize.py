import re

def filter(raw, canary):
    return  re.search('.....BEGIN PGP SIGNED MESSAGE.....(.*).....BEGIN PGP SIGNATURE.....', raw, re.DOTALL).group(1)
