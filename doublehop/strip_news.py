import re

def filter(raw, canary):
    clean = re.sub(re.compile('Tweeted image from.+$', re.DOTALL), '', raw)
    return clean
