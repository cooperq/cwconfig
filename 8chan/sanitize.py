import re

def filter(raw_text, canary):
    return  re.search('We have not been served any secret court orders and are not under any gag orders.', raw_text, re.DOTALL).group(0)

