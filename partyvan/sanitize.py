import re

def filter(raw, canary):
    return re.search('.....BEGIN PGP SIGNED MESSAGE......*.....END PGP SIGNATURE.....', raw, re.DOTALL).group(0)

