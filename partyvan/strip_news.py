import re

def filter(raw, canary):
    clean = re.sub(re.compile('The below content is an excerpt from .+$', re.DOTALL), '', raw)
    return clean
