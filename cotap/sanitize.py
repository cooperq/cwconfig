import re
from pyquery import PyQuery as pq

def filter(raw_text, canary):
    d = pq(raw_text) #d is like $ in jquery
    content = d('.medium-8').text()
    content = re.search('National Security Requests.+any changes.', content, re.DOTALL).group(0)
    return content

